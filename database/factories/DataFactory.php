<?php

use Faker\Generator as Faker;


$factory->define(App\Oprator::class, function (Faker $faker) {
      return [
            'username'             => 'oprator',
            'password'             => bcrypt('2647187'),
            'remember_token' => str_random(10),
      ];
});

$factory->define(App\Specialist::class, function (Faker $faker) {
      return [
            'speciality' => $faker->city,
      ];
});

$factory->define(App\Doctor::class, function (Faker $faker) {
      return [
            'name'                  => $faker->name,
            'phone_number'  => $faker->numberBetween(9300338494 , 9380338494),
            'specialist_id'       => App\Specialist::all()->random()->id,
      ];
});

$factory->define(App\Patient::class, function (Faker $faker) {
      return [
            'name'                  => $faker->name,
            'done'                  => 0 ,
            'phone_number'  => $faker->numberBetween(9200338494 , 9380338494),
            'date_time'           => $faker->dateTimeBetween('-1 week'),
            'dr_id'                  => App\Doctor::all()->random()->id,
      ];
});

$factory->define(App\WorkTime::class, function (Faker $faker) {
      return [
            'dr_id'             => App\Doctor::all()->random()->id,
            'day_id'           => $faker->numberBetween(0 , 6),
            'day'             => $faker->dayOfWeek,
            'start'              => '10:00:00',
            'end'               => '12:00:00',
            'visit_time'     => 10,
      ];
});
$factory->define(App\Off::class, function (Faker $faker) {
      return [
            'dr_id'    => App\Doctor::all()->random()->id,
            'date'      => $faker->dateTimeBetween('-2 week'),
            'jalaliDate' => ''
      ];
});