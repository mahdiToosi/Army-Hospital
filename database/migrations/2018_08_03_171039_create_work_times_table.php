<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkTimesTable extends Migration
{
      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up()
      {
            Schema::create('work_times', function (Blueprint $table) {
                  $table->increments('id');
                  $table->integer('dr_id');
                  $table->time('start');
                  $table->time('end');
                  $table->integer('visit_time');
                  $table->integer('day_id');
                  $table->char('day');
                  $table->timestamps();
                  
            });
      }
      
      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down()
      {
            Schema::drop('work_times');
      }
}
