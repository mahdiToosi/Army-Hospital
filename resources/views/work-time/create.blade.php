@extends('admin.layout.auth')

@section('content')
      <div class="container">
            <div class="row">

                  <div class="col-md-10">
                        <div class="card">
                              <div class="card-body col-lg-offset-4">
                                    <a href="{{ url('/work-time') }}" title="Back">
                                          <button class="btn btn-warning btn-sm">
                                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                          </button>
                                    </a>
                                    <br />
                                    <br />

                                    @if ($errors->any())
                                          <ul class="alert alert-danger">
                                                @foreach ($errors->all() as $error)
                                                      <li>{{ $error }}</li>
                                                @endforeach
                                          </ul>
                                    @endif

                                    {!! Form::open(['url' => '/work-time', 'class' => 'form-horizontal', 'files' => true]) !!}

                                    @include ('work-time.form')

                                    {!! Form::close() !!}

                              </div>
                        </div>
                  </div>
            </div>
      </div>
@endsection
