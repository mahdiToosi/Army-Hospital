@extends('admin.layout.auth')

@section('content')
      <div class="container">
            <br>
            <br>
            <br>
            <div class="row">
                  <div class="col-md-6 col-lg-offset-3">
                        <div class="card">
                              <div class="card-body">
                                    <div class="row col-lg-offset-2" style="direction: rtl">

                                          {!! Form::open(['url' => '/specialist', 'class' => 'form-horizontal', 'files' => true]) !!}
                                          <div class=" col-md-2">
                                                {!! Form::submit('Create', ['class' => 'btn btn-success']) !!}
                                          </div>
                                          <div class="col-md-6">

                                                {!! Form::text('speciality', null, ('' == 'required') ?
                                                ['class' => 'form-control  text-right', 'required' => 'required' , 'autofocus'] :
                                                ['class' => 'form-control text-right' , 'autofocus']) !!}

                                                {!! $errors->first('speciality', '<p class="help-block">:message</p>') !!}
                                          </div>
                                          {!! Form::label('speciality', 'تخصص', ['class' => 'col-md-2 control-label']) !!}

                                          {!! Form::close() !!}

                                    </div>
                                    <br/>
                                    <br/>
                                    <div class="table-responsive">
                                          <table class="table table-borderless">
                                                <thead>
                                                <tr>
                                                      <th class="text-center">#</th>
                                                      <th class="text-center"><strong>تخصص ها</strong> </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($specialist as $item)
                                                      <tr>
                                                            <td class="text-center">{{ $loop->iteration or $item->id }}</td>
                                                            <td class="text-center">{{ $item->speciality }}</td>
                                                            <td class="text-center">
                                                                  {!! Form::open([
                                                                      'method'=>'DELETE',
                                                                      'url' => ['/specialist', $item->id],
                                                                      'style' => 'display:inline'
                                                                  ]) !!}
                                                                  {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> حذف', array(
                                                                          'type' => 'submit',
                                                                          'class' => 'btn btn-danger btn-sm',
                                                                          'title' => 'Delete Specialist',
                                                                          'onclick'=>'return confirm("Confirm delete?")'
                                                                  )) !!}
                                                                  {!! Form::close() !!}
                                                            </td>
                                                      </tr>
                                                @endforeach
                                                </tbody>
                                          </table>
                                          <div class="pagination-wrapper"> {!! $specialist->appends(['search' => Request::get('search')])->render() !!} </div>
                                    </div>

                              </div>
                        </div>
                  </div>
            </div>
      </div>
@endsection
