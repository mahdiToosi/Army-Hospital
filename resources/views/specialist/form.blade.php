<div class="form-group {{ $errors->has('speciality') ? 'has-error' : ''}}">
    {!! Form::label('speciality', 'Speciality', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('speciality', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('speciality', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
