@extends('admin.layout.auth')

@section('content')
      <div class="container">
            <div class="row">
                  <br>
                  <br>
                  <div class="col-md-10">
                        <div class="card">
                              <div class="card-body">
                                    <div class="row col-lg-offset-3">
                                          {!! Form::open(['url' => '/off', 'class' => 'form-horizontal', 'files' => true]) !!}
                                          <div class="col-md-2">
                                                {!! Form::submit( 'ثبت کردن', [
                                                      'class' => 'btn btn-success' , 
                                                      'onclick'=>'return confirm("به بیمار های این تاریخ اطلاع داده اید ؟ ")'
                                                      ]) !!}
                                          </div>
                                          <div class="col-md-4">

                                                <date-picker
                                                      v-model="createOffDate"
                                                      format="YYYY-MM-DD jYYYY/jMM/jDD"
                                                      display-format="dddd  jDD  jMMMM  jYYYY"
                                                      placeholder="انتخاب تاریخ"
                                                >
                                                </date-picker>
                                                
                                                <input type="hidden" v-model="createOffDate" name="date">
                                          </div>
                                          {!! Form::label('end', 'تاریخ', ['class' => 'col-md-1 control-label']) !!}
                                          <div class="col-md-4">
                                                
                                                <select name="dr_id" class="form-control">
                                                      @foreach($docs as $doc)
                                                            <option value="{{$doc->id}}">{{$doc->name}}</option>
                                                      @endforeach
                                                </select>
                                          
                                          </div>
                                          
                                          {!! Form::label('dr_id', 'دکتر', ['class' => 'col-md-1 control-label']) !!}
                                          {!! Form::close() !!}
                                    
                                    </div>
                                    <br/>
                                    <br/>
                                    <div class="table-responsive col-lg-offset-3">
                                          <table class="table table-borderless">
                                                <thead>
                                                <tr>
                                                      <th class="text-center">#</th>
                                                      <th class="text-center">تاریخ</th>
                                                      <th class="text-center">دکتر</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($off as $item)
                                                      <tr>
                                                            <td class="text-center">{{ $loop->iteration or $item->id }}</td>
                                                            <td class="text-center">{{ $item->jalaliDate }}</td>
                                                            <td class="text-center">{{ $item->doctor->name }}</td>
                                                            <td class="text-center">
                                                                  {!! Form::open([
                                                                  'method'=>'DELETE',
                                                                  'url' => ['/off', $item->id],
                                                                  'style' => 'display:inline'
                                                                  ]) !!}
                                                                  {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> حذف', array(
                                                                        'type' => 'submit',
                                                                        'class' => 'btn btn-danger btn-sm',
                                                                        'title' => 'Delete Off',
                                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                                  )) !!}
                                                                  {!! Form::close() !!}
                                                            </td>
                                                      </tr>
                                                @endforeach
                                                </tbody>
                                          </table>
                                          <div class="pagination-wrapper"> {!! $off->appends(['search' => Request::get('search')])->render() !!} </div>
                                    </div>

                              </div>
                        </div>
                  </div>
            </div>
      </div>
@endsection
