export const InitDoctorsPerDay= ({commit})=>{
      axios.get('/get/Doctors/today')
            .then((response) => {
                  commit('SetDoctorsPerDay', response.data)
            })
};
export const   getDoctorsOptions= ({commit , state}) => {
      let specialityId = state.form.spaciality.speciality_id
      let url = '/get/Doctors/withSpecialityId/'+ specialityId
      axios.get(url).then((response) => {
                  commit('SetDoctorsData', response.data)
            })
};
export const   InitSpecialitys= ({commit}) =>{
      axios.get('/get/Specialitys')
            .then((response) => {
                  commit('SetSpecialitysData', response.data)
            })
};
export const  clickedOnDoctor= ({commit , dispatch} , doctorData) => {
      commit('SetSpacialityModalData', doctorData)
};
export const  getPatients = ({commit  , state}) =>{
      state.docSituationForModal = true
      let form = state.form
      if(form.date && form.doctor){
            let date = form.date + ' 00:00:00'
            let addr = '/get/patients/'+ date +'/'+ form.doctor.id
            // console.log(addr)
            axios.get(addr)
                  .then(response => {commit('SetPatientsData', response.data)})
      }
};
export const setFormName = ({commit} , name) =>{
      commit('setFormName' , name)
};
export const setFormNumber = ({commit} , num) =>{
      commit('setFormNumber' , num)
};
export const addPatient= ({state , commit , dispatch})=>{
      let date_time = state.form.date +' '+ state.form.time.time
      // console.log(date_time)
      let form= {
            name : state.form.name,
            phone_number: state.form.phone_number,
            date_time: date_time,
            dr_id: state.form.doctor.id,
            done: 0
      }
      axios.post('/patient', form)
            .then(function (response) {
                  // console.log(response);
            })
      commit('clearForm')
      dispatch('getPatients')

}