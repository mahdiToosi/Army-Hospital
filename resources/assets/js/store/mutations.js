import {eventBus} from "../app";

export const SetDoctorsPerDay = (state , DoctorsPerDay ) =>{
      if (DoctorsPerDay === 0){
            eventBus.$emit('todayWeDontHaveDoc')
      } else {
            eventBus.$emit('todayWeHaveDoc')
            state.DoctorsPerDay = DoctorsPerDay;
      }
};
export const SetDoctorsData = (state , docs) =>{
      state.Doctors = docs
};
export const SetSpacialityModalData = (state , doctorData) =>{
      state.form.spaciality = doctorData
      state.form.doctor = doctorData
      eventBus.$emit('getDoctorsOptions')
};
export const SetSpecialitysData = (state , specialitys) =>{
      let specialitysList = []
      specialitys.forEach(function (speciality) {
            let specialityObject = {
                  id: speciality.id,
                  speciality: speciality.speciality,
                  speciality_id : speciality.id,
            }
            specialitysList.push(specialityObject)
      })
      state.AllSpecialitys = specialitysList
};
export const SetPatientsData = (state , patients) =>{
      if(patients){
            let patientsData =  [];
            let docFreeTimes = []
            for(let i = 0; i < patients.length; i++) {
                  if (patients[i].id){
                        let patient = {
                              doctor_id : patients[i].doctor_id,
                              id : patients[i].id,
                              name  : patients[i].name,
                              phone_number : patients[i].phone_number,
                              time : patients[i].timeDate.substr(11,5),
                              done : patients[i].done
                        }
                        patientsData.push(patient);
                  }else {
                        let freeTimes = {
                              time : patients[i].timeDate.substr(11,5),
                        }
                        docFreeTimes.push(freeTimes);
                  }
            };
            if (!patientsData.length) {
                  // console.log(patientsData.length)
                  eventBus.$emit('thereIsNoPatientYet');
            }else {
                  eventBus.$emit('thereIsPatient')
            }
            state.docFreeTimes = docFreeTimes
            state.patientsData = patientsData
            eventBus.$emit('doctorIsOn')

      }else{
            // state.docSituationForModal = false
            eventBus.$emit('doctorIsOff')
      }
};
export const setFormName = (state , val) =>{
      state.form.name = val
};
export const setFormNumber= (state , val) =>{
      state.form.phone_number = val
};
export const setTime = (state , time) =>{
      let timeWithSecond = time+':00'
      state.form.time = timeWithSecond
};
export const clearForm = (state)=>{
      state.form.name = null
      state.form.time = null
      state.form.phone_number = null
}