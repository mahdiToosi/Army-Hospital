import Vue from 'vue';
import Vuex from 'vuex';
import * as actions from './actions';
import * as mutations from './mutations';

Vue.use(Vuex);

export const store = new Vuex.Store({
      state:{
            DoctorsPerDay: [],
            Doctors: [],
            AllSpecialitys: [],
            form: {
                  name: null,
                  date: moment().date(Number).format("YYYY-MM-DD"),
                  time: [],
                  phone_number: null,
                  doctor: null,
                  spaciality: null
            },
            patientsData: null,
            docFreeTimes: [],
            docSituationForModal : true,
      },
      getters:{
            DoctorsPerDayData: (state) => {
                  return state.DoctorsPerDay
            },
            formData: (state) => {
                  return state.form
            },
            Doctors: (state) => {
                  return state.Doctors
            },
            allSpecialitys: (state) => {
                  return state.AllSpecialitys
            },
            patientsData: (state) => {
                  return state.patientsData
            },
            docFreeTimes: (state) => {
                  return state.docFreeTimes
            },
            docSituationForModal: (state) =>{
                  return state.docSituationForModal
            },

      },
      mutations,
      actions
});
