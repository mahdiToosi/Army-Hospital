<?php

namespace App\Http\Controllers\OpratorAuth;

use App\Oprator;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
      /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
      */

      use RegistersUsers;

      /**
       * Where to redirect users after login / registration.
       *
       * @var string
       */
      protected $redirectTo = '/oprator/home';

      /**
       * Create a new controller instance.
       *
       * @return void
       */
      public function __construct()
      {
            $this->middleware('admin');
      }

      /**
       * Get a validator for an incoming registration request.
       *
       * @param  array  $data
       * @return \Illuminate\Contracts\Validation\Validator
       */
      protected function validator(array $data)
      {
            return Validator::make($data, [
                  'username' => 'required|max:255|unique:oprators',
                  'password' => 'required|min:6|confirmed',
            ]);
      }

      /**
       * Create a new user instance after a valid registration.
       *
       * @param  array  $data
       * @return Oprator
       */
      protected function create(array $data)
      {
            return Oprator::create([
                  'username' => $data['username'],
                  'password' => bcrypt($data['password']),
            ]);
      }

      /**
       * Show the application registration form.
       *
       * @return \Illuminate\Http\Response
       */
      public function showRegistrationForm()
      {
            $oprators = Oprator::get()->all();
            return view('oprator.auth.register' ,compact('oprators'));
      }

      /**
       * Get the guard to be used during registration.
       *
       * @return \Illuminate\Contracts\Auth\StatefulGuard
       */
      protected function guard()
      {
            return Auth::guard('oprator');
      }
}
