<?php

namespace App\Http\Controllers;

use App\Doctor;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Off;
use Illuminate\Http\Request;

class OffController extends Controller
{
      public function __construct()
      {
            $this->middleware('admin');
      }
      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\View\View
       */
      public function index(Request $request)
      {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                  $off = Off::where('date', 'LIKE', "%$keyword%")
                        ->orWhere('dr_id', 'LIKE', "%$keyword%")
                        ->latest()->paginate($perPage);
            } else {
                  $off = Off::latest()->paginate($perPage);
            }
            $docs= Doctor::get()->all();

            return view('off.index', compact('off' ,'docs'));
      }

      /**
       * Show the form for creating a new resource.
       *
       * @return \Illuminate\View\View
       */
      public function create()
      {

            return view('off.create' ,compact('docs'));
      }

      /**
       * Store a newly created resource in storage.
       *
       * @param \Illuminate\Http\Request $request
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function store(Request $request)
      {
            $date = $request->date;
            $gregorianDate = substr($date , 0 , 10);
            $jalaliDate = substr($date , 11 , 10);
            $off = new Off();
            $off->dr_id = $request->dr_id;
            $off->jalaliDate = $jalaliDate;
            $off->date = $gregorianDate;
            $off->save();

            return redirect(route('off.index'));
      }

      /**
       * Display the specified resource.
       *
       * @param  int  $id
       *
       * @return \Illuminate\View\View
       */
      public function show($id)
      {
            $off = Off::findOrFail($id);

            return view('off.show', compact('off'));
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       *
       * @return \Illuminate\View\View
       */
      public function edit($id)
      {
            $off = Off::findOrFail($id);

            return view('off.edit', compact('off'));
      }

      /**
       * Update the specified resource in storage.
       *
       * @param \Illuminate\Http\Request $request
       * @param  int  $id
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function update(Request $request, $id)
      {

            $requestData = $request->all();

            $off = Off::findOrFail($id);
            $off->update($requestData);

            return redirect('off')->with('flash_message', 'Off updated!');
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function destroy($id)
      {
            Off::destroy($id);

            return redirect('off')->with('flash_message', 'Off deleted!');
      }
}
